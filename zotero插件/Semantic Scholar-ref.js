{
	"translatorID": "276cb34c-6861-4de7-a11d-c2e46fb8af28",
	"label": "Semantic Scholar-ref",
	"creator": "modified by will emchy",
	"target": "^https?://(www\\.semanticscholar\\.org/+|pdfs\\.semanticscholar\\.org/)",
	"minVersion": "3.0",
	"maxVersion": "",
	"priority": 100,
	"inRepository": true,
	"translatorType": 4,
	"browserSupport": "gcsibv",
	"lastUpdated": "2022-11-09 08:51:44"
}

/*
	***** BEGIN LICENSE BLOCK *****

	Copyright © 2017 Guy Aglionby
	
	This file is part of Zotero.

	Zotero is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Zotero is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Zotero. If not, see <http://www.gnu.org/licenses/>.

	***** END LICENSE BLOCK *****
*/

// See also https://github.com/zotero/translators/blob/master/BibTeX.js
var bibtex2zoteroTypeMap = {
	inproceedings: "conferencePaper",
	conference: "conferencePaper",
	article: "journalArticle"
};

async function detectWeb (doc,url) {
	if (url.includes("paper"))
	{
		return "journalArticle";
	}
	else if (url.includes("search"))
	{
		return "multiple";
	}

	return false;
	// let citation = ZU.xpathText(doc, '//pre[@class="bibtex-citation"]');
	// let type = citation.split('{')[0].replace('@', '');
	// if (type){
	// Z.debug(type)
	// return bibtex2zoteroTypeMap[type];
	// }
	// else
	// {
	// 	return 'mutiple'
	// }
}


// 解析搜索页的网址和题目
function getSearchResults(doc, url,itemInfo) {
	// Z.debug("in=============getSearchResults=============")
	
  var items = {};
  var found = false;
  var rows = ZU.xpath(doc, "//div[@class='result-page']//div[@class='cl-paper-row serp-papers__paper-row paper-row-normal']");
//   Z.debug(rows)
//   if (!rows.length > 0) rows = doc.querySelectorAll("div.mod-results-list div.item");
  var idx = 1
  for (let row of rows) {
	  Z.debug("searchPage===in=======for============")
	var title = ZU.xpath(row, ".//h2[@class='cl-paper-title']")[0].innerText;
	Z.debug(idx+"===================title===========")
	Z.debug(title)
	// Z.debug("---------------------------------------------")
	var id = 'https://www.semanticscholar.org'+ZU.xpath(row,".//a[@data-selenium-selector='title-link']/@href")[0]["value"]
	// var id = title.getAttribute("href") ? getIDFromURL(title.href) : getIDFromPage(row);
	Z.debug(id);
	items[id] = idx + " " + title;
	// var id = getIDFromURL(href);
	// Z.debug(id);
	itemInfo[id.url] = id;
	idx +=1
	// break
	Z.debug("out===========for================")
  		}
	
//   Z.debug(itemInfo);
Z.debug("out=============getSearchResults=============")
// Z.debug(items)
  return items;
}


//解析详情页
function getPaperResults(doc, url,itemInfo) {
	// Z.debug("in=============getPaperResults=============")
	//当前页的题目和链接
	var schemaTag = doc.querySelector("script.schema-data");
	var schemaObject = JSON.parse(schemaTag.innerHTML);
	var title = schemaObject["@graph"][1][0].name;

	var items = {};
	items[url]="paper-"+title;
	// Z.debug(items)
	var found = false;
	var rows = ZU.xpath(doc, "//div[@class='cl-paper-row citation-list__paper-row']");
	// var references_rows=ZU.xpath(doc,"//div[@class='cl-paper-row citation-list__paper-row']//a[@data-heap-citation-type='citedPapers']")
	// var citations_rows = ZU.xpath(doc,"//div[@class='cl-paper-row citation-list__paper-row']//a[@data-heap-citation-type='citingPapers']")
//   Z.debug(rows)
//   if (!rows.length > 0) rows = doc.querySelectorAll("div.mod-results-list div.item");

//文章的引用文章解析
  var idx = 1
  var refcount=1
  var citcount=1
  for (let row of rows) {
		Z.debug("citation_reference--in=======for============")
		// Z.debug(row)
		var title = ZU.xpath(row, ".//h3[@class='cl-paper-title']")[0].innerText;
		Z.debug(idx+"===================title===========")
		// Z.debug(title)
		// Z.debug("---------------------------------------------")
		//url
		// Z.debug(ZU.xpath(row,".//a[@data-heap-citation-type='citingPapers']")[0])
		var id = ZU.xpath(row,".//a[@data-heap-id='citation_title']")[0]["href"]
		var reference = ZU.xpath(row,".//a[@data-heap-citation-type='citedPapers']")[0]
		var citation = ZU.xpath(row,".//a[@data-heap-citation-type='citingPapers']")[0]
		// Z.debug(citation)
		// Z.debug(reference)
		if (citation!=null)
		{
			idx = citcount+"-citation-"
			citcount+=1
		}
		else if(reference!=null)
		{
			idx=refcount+"-reference-"
			refcount+=1
		}
		// var id = ZU.xpath(row,".//a[@data-heap-citation-type='citingPapers']")[0]["href"]
		// Z.debug(id);
		items[id] = idx + " " + title;
		// var id = getIDFromURL(href);
		// Z.debug(id);
		itemInfo[id.url] = id;
		idx +=1
	
		// var id = title.getAttribute("href") ? getIDFromURL(title.href) : getIDFromPage(row);
	
		// break
		Z.debug("citation_reference===out===========for================")
  }
//   Z.debug(itemInfo);
Z.debug("out=============getPaperResults=============")
// Z.debug(items)
  return items;
}


function doWeb(doc, url) {
	if (url.includes("search"))
	{
		// Z.debug("===========multiple=========")
		var itemInfo = {};
		var items = getSearchResults(doc, url,itemInfo);
		Z.debug(items)
		Z.selectItems(items, function(selectedItems) {
			if (selectedItems) ZU.processDocuments(Object.keys(selectedItems), scrape);
		});
	}
	else if (url.includes('pdfs.semanticscholar.org')) {
		let urlComponents = url.split('/');
		let paperId = urlComponents[3] + urlComponents[4].replace('.pdf', '');
		const API_URL = 'https://api.semanticscholar.org/';
		ZU.processDocuments(API_URL + paperId, scrpae);
	}
	//抓取paper详情页的citations和references
	else if (url.includes("paper"))
	{
		Z.debug("===========papermultiple=========")
		var itemInfo = {};
		var items = getPaperResults(doc, url,itemInfo);
		// Z.debug(items)
		Z.selectItems(items, function(selectedItems) {
			if (selectedItems) ZU.processDocuments(Object.keys(selectedItems), scrape);
		});

	}
	//原来的详情页抓取
	else {
		scrape(doc, url);
	}
}



function scrape(doc, url) {
	Z.debug("=========scrape============")
	let citation = ZU.xpathText(doc, '//pre[@class="bibtex-citation"]');
	Z.debug("citation===="+citation)
	let type = citation.split('{')[0].replace('@', '');
	Z.debug("type===="+type)
	const itemType = bibtex2zoteroTypeMap[type];

	var item = new Zotero.Item(itemType);

	// load structured schema data
	const schemaTag = doc.querySelector("script.schema-data");
	// Z.debug("schemaTag======"+schemaTag)
	const schemaObject = JSON.parse(schemaTag.innerHTML);
	Z.debug("schemaObject===="+schemaObject)
	const article = schemaObject["@graph"][1][0];
	Z.debug("article===="+article)

	item.title = article.name;
	item.abstractNote = article.abstract;

	if (article.author) {
		article.author.forEach((author) => {
			item.creators.push(ZU.cleanAuthor(author.name, 'author'));
		});
	}
	item.publicationTitle = article.publication;
	item.date = article.datePublished;

	// attachments
	item.attachments.push({
		url: url,
		title: "Semantic Scholar Link",
		mimeType: "text/html",
		snapshot: false
	});

	// if semantic scholar has a pdf as it's primary paper link it will appear in the about field
	
	if(article.about.url){
		const paperLink = article.about.url;
		if (paperLink.includes("pdfs.semanticscholar.org") || paperLink.includes("arxiv.org")) {
		item.attachments.push({
			url: paperLink,
			title: "Full Text PDF",
			mimeType: 'application/pdf'
		});
	}
	};
	

	// use the public api to retrieve more structured data
	const paperIdRegex = /\/(.{40})(\?|$)/;
	const paperId = paperIdRegex.exec(url)[1];
	Z.debug("paperId====="+paperId)
	const apiUrl = `https://api.semanticscholar.org/v1/paper/${paperId}?client=zotero_connect`;
	ZU.doGet(apiUrl, (data) => {
		let json = JSON.parse(data);
		item.DOI = json.doi;
		item.complete();
	});
}


/** BEGIN TEST CASES **/
var testCases = [
	{
		"type": "web",
		"url": "https://www.semanticscholar.org/paper/TectoMT%3A-Modular-NLP-Framework-Popel-Zabokrtsk%C3%BD/e1ea10a288632a4003a4221759bc7f7a2df36208",
		"items": [
			{
				"itemType": "conferencePaper",
				"title": "TectoMT: Modular NLP Framework",
				"creators": [
					{
						"firstName": "Martin",
						"lastName": "Popel",
						"creatorType": "author"
					},
					{
						"firstName": "Zdenek",
						"lastName": "Zabokrtský",
						"creatorType": "author"
					}
				],
				"date": "2010",
				"DOI": "10.1007/978-3-642-14770-8_33",
				"abstractNote": "In the present paper we describe TectoMT, a multi-purpose open-source NLP framework. It allows for fast and efficient development of NLP applications by exploiting a wide range of software modules already integrated in TectoMT, such as tools for sentence segmentation, tokenization, morphological analysis, POS tagging, shallow and deep syntax parsing, named entity recognition, anaphora resolution, tree-to-tree translation, natural language generation, word-level alignment of parallel corpora, and other tasks. One of the most complex applications of TectoMT is the English-Czech machine translation system with transfer on deep syntactic (tectogrammatical) layer. Several modules are available also for other languages (German, Russian, Arabic).Where possible, modules are implemented in a language-independent way, so they can be reused in many applications.",
				"libraryCatalog": "Semantic Scholar",
				"proceedingsTitle": "IceTAL",
				"shortTitle": "TectoMT",
				"attachments": [
					{
						"title": "Semantic Scholar Link",
						"mimeType": "text/html",
						"snapshot": false
					}
				],
				"tags": [],
				"notes": [],
				"seeAlso": []
			}
		]
	},
	{
		"type": "web",
		"url": "https://www.semanticscholar.org/paper/TectoMT%3A-Modular-NLP-Framework-Popel-%C5%BDabokrtsk%C3%BD/e1ea10a288632a4003a4221759bc7f7a2df36208",
		"items": [
			{
				"itemType": "conferencePaper",
				"title": "TectoMT: Modular NLP Framework",
				"creators": [
					{
						"firstName": "M.",
						"lastName": "Popel",
						"creatorType": "author"
					},
					{
						"firstName": "Z.",
						"lastName": "Žabokrtský",
						"creatorType": "author"
					}
				],
				"date": "2010",
				"DOI": "10.1007/978-3-642-14770-8_33",
				"abstractNote": "TectoMT allows for fast and efficient development of NLP applications by exploiting a wide range of software modules already integrated in TECToMT, such as tools for sentence segmentation, tokenization, morphological analysis, POS tagging, shallow and deep syntax parsing, named entity recognition, anaphora resolution, tree-to-tree translation, natural language generation, word-level alignment of parallel corpora, and other tasks. In the present paper we describe TectoMT, a multi-purpose open-source NLP framework. It allows for fast and efficient development of NLP applications by exploiting a wide range of software modules already integrated in TectoMT, such as tools for sentence segmentation, tokenization, morphological analysis, POS tagging, shallow and deep syntax parsing, named entity recognition, anaphora resolution, tree-to-tree translation, natural language generation, word-level alignment of parallel corpora, and other tasks. One of the most complex applications of TectoMT is the English-Czech machine translation system with transfer on deep syntactic (tectogrammatical) layer. Several modules are available also for other languages (German, Russian, Arabic).Where possible, modules are implemented in a language-independent way, so they can be reused in many applications.",
				"libraryCatalog": "Semantic Scholar",
				"proceedingsTitle": "IceTAL",
				"shortTitle": "TectoMT",
				"attachments": [
					{
						"title": "Semantic Scholar Link",
						"mimeType": "text/html",
						"snapshot": false
					}
				],
				"tags": [],
				"notes": [],
				"seeAlso": []
			}
		]
	}
]
/** END TEST CASES **/
