<center>zotero浏览器插件抓取知网等国内网页</center>

# 油猴方案

1. 将该仓库或者[百度网盘:提取码：cgym](https://pan.baidu.com/s/1gXJlZ2r7t9AZvatP6uNL1g)里面`translators_CN-master`的`translator`里面的`js`文件放到自己zotero数据库里面的`translator`文件夹，然后重启软件。

2. 刷新浏览器插件

   1. 右键点击浏览器插件，点击`选项`

      <img src="zotero浏览器插件抓取知网等国内网页.assets/image-20220320183859494.png" alt="image-20220320183859494" style="zoom:50%;" />

      2. `Advanced-Update Translators`，多点几下。

      ![image-20220320183926545](zotero浏览器插件抓取知网等国内网页.assets/image-20220320183926545.png)

3. 查看zotero里面的数据库所在位置

   `编辑-首选项-高级-文件和文件夹-数据存储位置`

![image-20220320183641418](zotero浏览器插件抓取知网等国内网页.assets/image-20220320183641418-16477726029551.png)

如果你没有更改你的数据库位置，就是默认那个。

# 海外知网方案

切换为海外版

![image-20220825154641858](zotero浏览器插件抓取知网等国内网页.assets/image-20220825154641858.png)

开启翻译

![image-20220825154648713](zotero浏览器插件抓取知网等国内网页.assets/image-20220825154648713.png)

抓取

![image-20220825154653939](zotero浏览器插件抓取知网等国内网页.assets/image-20220825154653939.png)